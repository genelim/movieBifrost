import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { MovieProvider } from '../../providers/movie/movie';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  movies: any = []
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private movie: MovieProvider,
    private modalController: ModalController
  ) {
    this.getMovies()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  getMovies(){
    this.movie.getMovies().subscribe(res => {
      this.movies = res
    })
  }

  add(){
    let modal = this.modalController.create('MovieAddPage')
    modal.present()
    modal.onDidDismiss(data => {
      this.movies.push({
        title: data.title,
        img: 'https://vignette.wikia.nocookie.net/ideas/images/e/e4/Movie_night.jpg/revision/latest?cb=20141222232947',
        actor: data.actor,
        year: data.year
      })
    });
  }
  
  view(movie){
    this.navCtrl.push('MovieViewPage', movie)
  }
}
