import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-movie-view',
  templateUrl: 'movie-view.html',
})
export class MovieViewPage {
  movie
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.movie = navParams.data
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MovieViewPage');
  }

}
