import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';

/**
 * Generated class for the MovieAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-movie-add',
  templateUrl: 'movie-add.html',
})
export class MovieAddPage {
  movieDetails
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private loadingController: LoadingController) {
    this.movieDetails = {}
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MovieAddPage');
  }

  dismiss(data) {
    console.log(data)
    this.viewCtrl.dismiss(data);
  }

  add(){
    let loader = this.loadingController.create({
      content: "Adding...",
      duration: 1000
    });
    loader.present(); 

    this.dismiss(this.movieDetails)
  }

}
