import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MovieAddPage } from './movie-add';

@NgModule({
  declarations: [
    MovieAddPage,
  ],
  imports: [
    IonicPageModule.forChild(MovieAddPage),
  ],
})
export class MovieAddPageModule {}
