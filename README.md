## Program Installation

Download and Install NodeJS. ( https://nodejs.org/en/ )
Install Ionic ( **npm install -g cordova ionic** )

## App Installation

Open terminal
- Go to this folder's directory ( **cd directory_name** ) 
- First time executing, run ( **npm install** ). This will install all the core dependencies needed. 

## Running App

From Terminal Run Either:
1. ionic serve (Open in full screen view)
2. ionic lab (Open in mobile view)

## API Reference

http://www.json-generator.com/api/json/get/ceETxzlagi?indent=2

## Contributors

Bifrost Members

## License

© Bifrost 2018